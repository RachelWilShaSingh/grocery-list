<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Grocery List </title>

  <link rel="stylesheet" href="assets/style.css">
  <link rel="icon" type="image/png" href="assets/favicon.png">
  
  
  <!-- jquery -->
  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  
  <!-- bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  
</head>

<body id="grocery-list">
    <? include_once( "logic.php" ); ?>
    
    <div class="container-fluid">
      <div class="row">
        
        <!-- header -->
        <div class="col-md-12">
          
          <div class="updater">
              <form method="post">
                  <p>
                      <span class="label">Food:</span><br>
                      <input type="text" class="form-control" name="newfood[name]" value="">
                  </p>
                  <p>
                      <span class="label">Type:</span><br>
                      <select name="newfood[type]" class="form-control">
                          <? foreach( $shoppingList->categories as $key => $category ) { ?>
                          <option value="<?=$category?>"><?=$category?></option>
                          <? } ?>
                      </select>

                  </p>
                  
                  <p>
                      <input type="submit" class="btn btn-primary form-control" name="add-new-food" value="Add">
                  </p>
              </form>
          </div>
          
        </div>
        <!-- header -->
        
        <div class="col-md-12"><hr></div>
        
        <!-- list -->
        <div class="col-md-12">
          <div class="food-list row cf">
              <? foreach( $shoppingList->foodList as $category => $items ) { ?>
                  <? if ( sizeof( $items ) == 0 ) { continue; } ?>
                  
                  <? if ( $category == "CHORES" ) { continue; } ?> 
                       
                  <div class="category col-md-2 col-sm-6">
                    <div class="card">
                      <div class="card-header">
                        <p><strong><?=$category?></strong></p>
                      </div>
                      <div class="card-body">
                        <? foreach( $items as $key => $item ) { ?>
                            <div class="food-item">
                              <form method="post">
                                  <input type="hidden" name="deletefood[name]" value="<?=$item?>">
                                  <input type="hidden" name="deletefood[type]" value="<?=$category?>">
                                  <input type="submit" name="delete-food" value="X" class="btn btn-danger">
                                  <span class="food-name"><?=$item?></span>
                              </form>                                
                            </div>
                        <? } ?>
                      </div>
                    </div>
                    <br>
                  </div>
              <? } /* for each item */ ?>
          </div>
          
        </div>
        <!-- list -->
        
        <div class="col-md-12"><hr></div>
        
        <!-- list -->
        <div class="col-md-12">
          <div class="food-list cf">
              <p><strong>Chores</strong></p>
              <? foreach ( $shoppingList->foodList["CHORES"] as $key=>$item ) { ?>
                  <div class="category">
                      <form method="post">
                          <input type="hidden" name="deletefood[name]" value="<?=$item?>">
                          <input type="hidden" name="deletefood[type]" value="<?=$category?>">
                          <input type="submit" name="delete-food" value="X" class="btn btn-danger">
                          <span class="food-name"><?=$item?></span>
                      </form>
                  </div>
              <? } ?>
          </div>
          
        </div>
        <!-- list --> 
        
        <div class="col-md-12"><hr></div>
        
      </div>
    </div>
  
</body>
</html>
