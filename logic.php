<?
class ShoppingList
{
    public $foodList;
    public $path;
    public $categories;
    
    function __construct( $fields ) {
        $this->categories = $fields;
        $this->path = "data/list.json";
        
        $this->LoadList();
        
        if ( isset( $_POST["add-new-food"] ) ) {
            $this->AddFood( $_POST["newfood"] );
        }
        else if ( isset( $_POST["delete-food"] ) ) {
            $this->RemoveFood( $_POST["deletefood"] );
        }
    }
    
    function AddFood( $info ) {
        array_push( $this->foodList[ $info["type"] ], $info["name"] );
        $this->SaveList();
    }
    
    function RemoveFood( $info ) {
        // What index is this food?
        $index = array_search( $info["name"], $this->foodList[ $info["type"] ] );
        unset( $this->foodList[ $info["type"] ][ $index ] );
        $this->SaveList();
    }
    
    function InitList() {
        $this->foodList = array();
        
        foreach ( $this->categories as $key => $category ) {
            $this->foodList[ $category ] = array();
        }
        
        $this->SaveList();
    }
    
    function SaveList() {
        $jsonString = json_encode( $this->foodList );
        file_put_contents( $this->path, $jsonString );
    }
    
    function LoadList() {
        if ( !file_exists( $this->path ) ) {
            $this->InitList();
        }
        $jsonString = file_get_contents( $this->path );
        $this->foodList = json_decode( $jsonString, true );
    }
}

$shoppingList = new ShoppingList( 
  array( 
    "Produce", 
    "Bakery",
    "Bread/Condiments",
    "Soups/Canned", 
    "Boxed/Pasta", 
    "Breakfast/Cereal", 
    "Coffee/Tea", 
    "Snacks/Chips/Candy", 
    "Juice/Soda/Drink", 
    "Frozen", 
    "Dairy",  
    "Storage", 
    "Cleaning", 
    "Misc", 
    "Indian Grocery",
    "BREAKFAST IDEAS", 
    "LUNCH/DINNER IDEAS", 
    "CHORES" 
    ) 
  );
?>
